﻿
using CaiKeCore.Domain;
using Microsoft.EntityFrameworkCore;


namespace CaiKeCore.Date
{
   public class CaiKeCodrContext : DbContext
    {

        public CaiKeCodrContext(DbContextOptions<CaiKeCodrContext> options) : base(options)
        {

        }


        public DbSet<Sys_Area> Sys_Area { get; set; }
        public DbSet<Sys_DbBackup> Sys_DbBackup { get; set; }
        public DbSet<Sys_FilterIP> Sys_FilterIp { get; set; }
        public DbSet<Sys_ItemsDetail> Sys_ItemsDetail { get; set; }
        public DbSet<Sys_Items> Sys_Items { get; set; }
        public DbSet<Sys_Log> Sys_Log { get; set; }
        public DbSet<Sys_Module> Sys_Module { get; set; }
        public DbSet<Sys_ModuleButton> Sys_ModuleButton { get; set; }
        public DbSet<Sys_Organize> Sys_Organize { get; set; }
        public DbSet<Sys_RoleAuthorize> Sys_RoleAuthorize { get; set; }
        public DbSet<Sys_Role> Sys_Role { get; set; }
        public DbSet<Sys_User> Sys_User { get; set; }
        public DbSet<Sys_Users> Sys_Users { get; set; }

        public DbSet<WaterHisData> WaterHisData { get; set; }
        public DbSet<StationInfo> StationInfo { get; set; }

        public DbSet<StationImageData> StationImageData { get; set; }

        public DbSet<DataPackState> DataPackState { get; set; }
        public DbSet<DeviceState> DeviceState { get; set; }
        public DbSet<PublicWaterFlowRelation> PublicWaterFlowRelation { get; set; }
        public DbSet<RainfallData> RainfallData { get; set; }

        public DbSet<TaskProject> TaskProject { get; set; }


        public DbSet<ST_WATER_HOUR> ST_WATER_HOUR { get; set; }

        public DbSet<ST_ZQRL> ST_ZQRL { get; set; }

    }
}
