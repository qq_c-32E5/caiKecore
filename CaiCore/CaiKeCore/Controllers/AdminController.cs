﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CaiKeCore.Controllers
{
    public class AdminController : Controller
    {
        [Authorize]
        public IActionResult Index() => View();
        [Authorize]
        public IActionResult Users() => View();
        [Authorize]
        public IActionResult UserForm() => View();
        [Authorize]
        public IActionResult Image() => View();
        [Authorize]
        public IActionResult Water() => View();
        [Authorize]
        public IActionResult WaterForm() => View();
        [Authorize]
        public IActionResult Waters() => View();
        [Authorize]
        public IActionResult Module() => View();
        [Authorize]
        public IActionResult ModuleForm() => View();


    }
}