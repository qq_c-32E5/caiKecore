﻿using System;
using CaiKeCore.Data.Repository;
using CaiKeCore.Domain;
using CaiKeCore.Code;
using Microsoft.AspNetCore.Mvc;
using CaiKeCore.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CaiKeCore.Controllers.API
{
    [ApiController]
    [Route("api/[controller]")]
    public class ImageController : Controller
    {
        public IRepositoryBase<StationImageData> Repository { get; }

        public ImageController(IRepositoryBase<StationImageData> repository)
        {
            Repository = repository;
        }
        // GET: api/<controller>
        [HttpPost]
        public object Post(SearchModel model)
        {

          var list=  Repository.Query(t=>t.DeviceID== model.selectid + "91"&&t.CollectionTime>= model.stardate&& t.CollectionTime<= model.enddate).Result;

            return list.ToJson();
        }

    }
}
