﻿using System.Collections.Generic;
using CaiKeCore.Data.Repository;
using CaiKeCore.Domain;
using CaiKeCore.Code;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CaiKeCore.Controllers.API
{
    [ApiController]
    [Route("api/[controller]")]
    public class StationInfoController : Controller
    {
        public IRepositoryBase<StationInfo> Repository { get; }

        public StationInfoController(IRepositoryBase<StationInfo> repository)
        {
            Repository = repository;
        }
        // GET: api/<controller>
   
        [HttpGet]
        public object Get()
        {
            var list = Repository.Query(t=>t.IsMainDevice==1).Result;
            return list.ToJson();
        }

        // GET api/<controller>/5
     
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }


        // GET: api/<controller>
   
        [HttpGet("GetLists")]
        public object GetLists()
        {
            var list = Repository.Query().Result;
            return list.ToJson();
        }

    }
}
