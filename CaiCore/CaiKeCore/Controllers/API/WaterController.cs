﻿using CaiKeCore.Data.Repository;
using CaiKeCore.Domain;
using CaiKeCore.Code;
using CaiKeCore.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CaiKeCore.Controllers.API
{
    [ApiController]
    [Route("api/[controller]")]
    public class WaterController : Controller
    {
        public IRepositoryBase<WaterHisData> Water { get; }
        public IRepositoryBase<ST_WATER_HOUR> Stwater { get; }
        public IRepositoryBase<StationInfo> Station { get; }

        public WaterController(IRepositoryBase<WaterHisData> water, IRepositoryBase<ST_WATER_HOUR> stwater,IRepositoryBase<StationInfo> station)
        {
            Water = water;
            Stwater = stwater;
            Station = station;
        }
        // GET: api/add/<controller>
        [Authorize]
        [HttpPost("Add")]
        public object Add(WaterAddModel model)
        {
            var addwater = new ST_WATER_HOUR
            {
              
                DeviceID = model.DeviceID,
                TM = model.TM,
                Water = Convert.ToDecimal(model.Water),
                Flow = Convert.ToDecimal(model.Flow)

            };
            var ret = Stwater.Add(addwater).Result>0?true:false;

            return ret.ToJson();
        }

        // GET api/Eight/<controller>    获取最新和当天早8点水情
     
        [HttpGet("GetEight")]
        public object GetEight()
        {            
            List<WaterViewModel> Model = new List<WaterViewModel>();       
            var stalist = Station.Query(t=>t.IsMainDevice==1).Result;
            var dayTime =Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd")+ " 08:00:00");
            for (int i = 0; i < stalist.Count; i++)
            {
                WaterViewModel model = new WaterViewModel();
                var Stlist = Stwater.Query(t=>t.DeviceID==stalist[i].DeviceID+"1",t=>t.TM,100).Result;
                var MaxDate = Stlist.Max(t => t.TM);
                foreach (var item in Stlist)
                {
                    if (item.TM== MaxDate)
                    {
                        model.Water = item.Water;
                        model.Flow = item.Flow;
                    }
                    if (item.TM == dayTime)
                    {
                        model.Water8 = item.Water;
                        model.Flow8 = item.Flow;                    
                    }
                }
                Model.Add(new WaterViewModel
                {   STName= stalist[i].StationName,
                    DeviceID = stalist[i].DeviceID,
                    Flow = model.Flow>0? model.Flow:0M,
                    Water = model.Water > 0 ? model.Water : 0M,
                    Flow8 = model.Flow8 > 0 ? model.Flow8 : 0M,
                    Water8 = model.Water8 > 0 ? model.Water8 : 0M,
                    TM = Stlist[i].TM,
                    TM8= dayTime
                });
            }

            var ret = new { code=0,msg="",data= Model ,count= Model.Count};
            return ret.ToJson();
        }

        // GET api/<controller>
      
        [HttpPost("Waterlist")]
        public object Waterlist(SearchModel model)
        {
            var list = Water.Query(t=>t.DeviceID== model.selectid+ "1"&&t.CollectionTime>= model.stardate&& t.CollectionTime<=model.enddate).Result;
            return list.ToJson();
        }

        // GET api/<controller>
      
        [HttpPost("Getwater")]
        public object Getwater(Paginated paginated)
        {
            var list = Stwater.Query(t => t.DeviceID == paginated.selectid + "1" && t.TM >= paginated.stardate && t.TM <= paginated.enddate, paginated).Result;
            var ret = new { code = 0, msg = "", data = list, count = paginated.records };
            return ret.ToJson();
        }
        // POST api/<controller>
        [Authorize]
        [HttpPut]
        public object Put(WaterAddModel Model)
        {
            var list = new ST_WATER_HOUR
            {
                Id =Convert.ToInt32(Model.Id),
                DeviceID = Model.DeviceID,
                TM = Model.TM,
                Water =Convert.ToDecimal(Model.Water),
                Flow = Convert.ToDecimal(Model.Flow)
            };
            var ret = Stwater.Update(list).Result;
            return ret.ToJson();
        }

        // DELETE api/<controller>/5
        [Authorize]
        [HttpDelete("{id}")]
        public object Delete(int Id)
        {
            var ret = Stwater.DeleteById(Id).Result;
            return ret.ToJson();
        }
    }
}
