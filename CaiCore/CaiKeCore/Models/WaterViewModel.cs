﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaiKeCore.Models
{
    public class WaterViewModel
    {
        public int Id { get; set; }
        public string STName { get; set; }
        public string DeviceID { get; set; }
        public DateTime TM { get; set; }
        public decimal? Water { get; set; }
        public decimal? Flow { get; set; }
        public DateTime TM8 { get; set; }
        public decimal? Water8 { get; set; }
        public decimal? Flow8 { get; set; }
    }
}
