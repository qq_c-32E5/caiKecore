﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaiKeCore.Models
{
    public class WaterAddModel
    {
        public string Id { get; set; }
        public string DeviceID { get; set; }
        public DateTime TM { get; set; }
        public string Water { get; set; }
        public string Flow { get; set; }
    
    }
}
