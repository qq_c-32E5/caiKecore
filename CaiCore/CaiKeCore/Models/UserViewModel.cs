﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaiKeCore.Models
{
    public class UserViewModel
    {
        public string F_Id { get; set; }
        public string F_Account { get; set; }
        public string F_UserPassword { get; set; }
        public string F_RealName { get; set; }
        public string F_Gender { get; set; }
        public string F_MobilePhone { get; set; }
        public string F_ManagerId { get; set; }
        public string F_IsAdministrator { get; set; }
        public string F_EnabledMark { get; set; }

        public DateTime? F_CreatorTime { get; set; }


    }
}
