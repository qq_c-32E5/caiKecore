﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaiKeCore.Models
{
    public class SearchModel
    {
        public string selectid { get; set; }
        public DateTime stardate { get; set; }
        public DateTime enddate { get; set; }
    }
}
