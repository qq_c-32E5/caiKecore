﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CaiKeCore.Domain
{
    public  class DataPackState
    {
        [Key]
        public string DataPackStateID { get; set; }
        public string DeviceID { get; set; }
        public string PhoneNumber { get; set; }
        public string DeviceTypeName { get; set; }
        public int? PackSendIndex { get; set; }
        public int? PackState { get; set; }
        public int? CommandID { get; set; }
        public string SendType { get; set; }
        public string PackType { get; set; }
        public DateTime? RecTime { get; set; }
        public string MainDisplay { get; set; }
        public string PackData { get; set; }
    }
}
