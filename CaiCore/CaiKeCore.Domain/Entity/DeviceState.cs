﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CaiKeCore.Domain
{
    public  class DeviceState
    {
        [Key]
        public string DeviceStateID { get; set; }
        public string DeviceID { get; set; }
        public decimal? SolarPannelsVoltage { get; set; }
        public decimal? BatteryVoltage { get; set; }
        public decimal? SolarPannelsElectricCurrent { get; set; }
        public decimal? BatteryElectricCurrent { get; set; }
        public DateTime CollectionTime { get; set; }
        public decimal? OtherState1 { get; set; }
        public decimal? OtherState2 { get; set; }
        public decimal? OtherState3 { get; set; }
        public decimal? OtherState4 { get; set; }
        public string OtherStateText { get; set; }
    }
}
