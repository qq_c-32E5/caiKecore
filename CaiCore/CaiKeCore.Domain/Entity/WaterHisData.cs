﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CaiKeCore.Domain
{
    public  class WaterHisData
    {
        [Key]
        public string WaterHisDataID { get; set; }
        public string DeviceID { get; set; }
        public decimal Water { get; set; }
        public decimal? Flow { get; set; }
        public decimal? Flows { get; set; }
        public DateTime CollectionTime { get; set; }
        public short WaterFlowType { get; set; }
        public DateTime? UpdateTime { get; set; }
        public decimal? Speed { get; set; }
    }
}
