﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CaiKeCore.Domain
{
    public  class StationImageData
    {
        [Key]
        public string StationImageDataID { get; set; }
        public string DeviceID { get; set; }
        public string ImageName { get; set; }
        public string ImagePath { get; set; }
        public DateTime CollectionTime { get; set; }
        public DateTime? UpdateTime { get; set; }
    }
}
