﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CaiKeCore.Domain
{
    public class Sys_RoleAuthorize
    {
        [Key]
        public string F_Id { get; set; }
        public int? F_ItemType { get; set; }
        public string F_ItemId { get; set; }
        public int? F_ObjectType { get; set; }
        public string F_ObjectId { get; set; }
        public int? F_SortCode { get; set; }
        public DateTime? F_CreatorTime { get; set; }
        public string F_CreatorUserId { get; set; }
    }
}
