﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CaiKeCore.Domain
{
    public  class RainfallData
    {
        [Key]
        public string DeviceID { get; set; }
        public System.DateTime CollectionTime { get; set; }
        public string RainfallType { get; set; }
        public decimal? RainfallValue { get; set; }
        public int? RainRC { get; set; }
        public DateTime? UpDateTime { get; set; }
    }
}
