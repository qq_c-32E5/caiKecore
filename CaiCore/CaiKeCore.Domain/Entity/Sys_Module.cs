﻿using System;

using System.ComponentModel.DataAnnotations;

namespace CaiKeCore.Domain
{
   public class Sys_Module
    {
        [Key]
        [DataType("模块主键")]
        public string F_Id { get; set; }
        [DataType("父级")]
        public string F_ParentId { get; set; }
        [DataType("层次")]
        public int? F_Layers { get; set; }
        [DataType("编码")]
        public string F_EnCode { get; set; }
        [DataType("名称")]
        public string F_FullName { get; set; }
        [DataType("图标")]
        public string F_Icon { get; set; }
        [DataType("连接")]
        public string F_UrlAddress { get; set; }
        [DataType("目标")]
        public string F_Target { get; set; }
        [DataType("菜单")]
        public bool? F_IsMenu { get; set; }
        [DataType("展开")]
        public bool? F_IsExpand { get; set; }
        [DataType("公共")]
        public bool? F_IsPublic { get; set; }
        [DataType("允许编辑")]
        public bool? F_AllowEdit { get; set; }
        [DataType("允许删除")]
        public bool? F_AllowDelete { get; set; }
        [DataType("排序码")]
        public int? F_SortCode { get; set; }
        [DataType("删除标志")]
        public bool? F_DeleteMark { get; set; }
        [DataType("有效标志")]
        public bool? F_EnabledMark { get; set; }
        [DataType("描述")]
        public string F_Description { get; set; }
        [DataType("创建日期")]
        public DateTime? F_CreatorTime { get; set; }
        [DataType("创建用户主键")]
        public string F_CreatorUserId { get; set; }
        [DataType("最后修改时间")]
        public DateTime? F_LastModifyTime { get; set; }
        [DataType("最后修改用户")]
        public string F_LastModifyUserId { get; set; }
        [DataType("删除时间")]
        public DateTime? F_DeleteTime { get; set; }
        [DataType("删除用户")]
        public string F_DeleteUserId { get; set; }

    }
}
