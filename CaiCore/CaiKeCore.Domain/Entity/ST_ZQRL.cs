﻿using System.ComponentModel.DataAnnotations;

namespace CaiKeCore.Domain
{
    public class ST_ZQRL
    {
        [Key]
        public int Id { get; set; }
        public string DeviceID { get; set; }

        public decimal W { get; set; }

        public decimal F { get; set; }

    }
}
