﻿using System;
using System.ComponentModel.DataAnnotations;

using System.Text;

namespace CaiKeCore.Domain
{
 public  class StationInfo
    {
        [Key]
        public int ID { get; set; }
        public string StationID { get; set; }
        public string DeviceID { get; set; }
        public string StationName { get; set; }
        public string DeviceType { get; set; }
        public decimal? PointX { get; set; }
        public decimal? PointY { get; set; }
        public string DeviceParamet { get; set; }
        public string CommunicationPort { get; set; }
        public decimal? Elevation { get; set; }
        public short? IsMainDevice { get; set; }
        public string IntallAddr { get; set; }
        public string Remark { get; set; }
        public string ParentDeviceID { get; set; }
        public int? PackSendIndex { get; set; }
        public string DeviceTypeDetail { get; set; }
        public string OtherProperty { get; set; }
        public int? childchannelnum { get; set; }
        public int? channelnumber { get; set; }
        public decimal? BaseFlowValue { get; set; }
        public string ChannelType { get; set; }
        public string DeviceCode { get; set; }
        public DateTime? DeviceTime { get; set; }
    }
}
