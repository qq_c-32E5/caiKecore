﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CaiKeCore.Domain
{
   public class ST_WATER_HOUR
    {

        [Key]
        public int Id { get; set; }
        public string DeviceID { get; set; }
        public DateTime TM { get; set; }
        public decimal? Water { get; set; }
        public decimal? Flow { get; set; }
    }
}
