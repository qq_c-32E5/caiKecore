﻿using System;
using System.ComponentModel.DataAnnotations;


namespace CaiKeCore.Domain
{
    public class Sys_Role
    {
        [Key]
        [DataType("角色主键")]
        public string F_Id { get; set; }
        [DataType("组织主键")]
        public string F_OrganizeId { get; set; }
        [DataType("分类1-角色2-岗位")]
        public int? F_Category { get; set; }
        [DataType("编号")]
        public string F_EnCode { get; set; }
        [DataType("名称")]
        public string F_FullName { get; set; }
        [DataType("类型")]
        public string F_Type { get; set; }
        [DataType("允许编辑")]
        public bool? F_AllowEdit { get; set; }
        [DataType("允许删除")]
        public bool? F_AllowDelete { get; set; }
        [DataType("角色主键")]
        public int? F_SortCode { get; set; }
        [DataType("角色主键")]
        public bool? F_DeleteMark { get; set; }
        [DataType("角色主键")]
        public bool? F_EnabledMark { get; set; }
        [DataType("角色主键")]
        public string F_Description { get; set; }
        [DataType("角色主键")]
        public DateTime? F_CreatorTime { get; set; }
        [DataType("角色主键")]
        public string F_CreatorUserId { get; set; }
        [DataType("角色主键")]
        public DateTime? F_LastModifyTime { get; set; }
        [DataType("角色主键")]
        public string F_LastModifyUserId { get; set; }
        [DataType("角色主键")]
        public DateTime? F_DeleteTime { get; set; }
        [DataType("角色主键")]
        public string F_DeleteUserId { get; set; }

    }
}
