﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CaiKeCore.Domain
{
    public  class TaskProject
    {
        [Key]
        public string TaskProjectID { get; set; }
        public string TaskProjectName { get; set; }
        public byte? TaskProjectStyle { get; set; }
        public string Remark { get; set; }
    }
}
