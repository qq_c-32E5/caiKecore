﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CaiKeCore.Domain
{
    public  class PublicWaterFlowRelation
    {
        [Key]
        public string WaterFlowRelationId { get; set; }
        public string DeviceID { get; set; }
        public decimal Water { get; set; }
        public decimal Flow { get; set; }
        public short IsChange { get; set; }
        public DateTime ChangeTime { get; set; }
        public bool? IsKeyPoint { get; set; }
    }
}
