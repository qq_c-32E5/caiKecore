﻿using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;

namespace CaiKeCore.Domain
{
   public class Sys_Users: IdentityUser
    {
        [Key]
        [DataType("用户主键")]
        public string F_Id { get; set; }
        [DataType("账户")]
        public string F_Account { get; set; }

        [DataType("用户密码")]
        public string F_UserPassword { get; set; }
        [DataType("姓名")]
        public string F_RealName { get; set; }
        [DataType("昵称")]
        public string F_NickName { get; set; }
        [DataType("头像")]
        public string F_HeadIcon { get; set; }
        [DataType("性别")]
        public string F_Gender { get; set; }
        [DataType("生日")]
        public DateTime? F_Birthday { get; set; }
        [DataType("手机")]
        public string F_MobilePhone { get; set; }
        [DataType("邮箱")]
        public string F_Email { get; set; }
        [DataType("微信")]
        public string F_WeChat { get; set; }
        [DataType("主管主键")]
        public string F_ManagerId { get; set; }
        [DataType("安全级别")]
        public int? F_SecurityLevel { get; set; }
        [DataType("个性签名")]
        public string F_Signature { get; set; }
        [DataType("组织主键")]
        public string F_OrganizeId { get; set; }
        [DataType("部门主键")]
        public string F_DepartmentId { get; set; }
        [DataType("角色主键")]
        public string F_RoleId { get; set; }
        [DataType("岗位主键")]
        public string F_DutyId { get; set; }
        [DataType("是否管理员")]
        public bool? F_IsAdministrator { get; set; }
        [DataType("排序码")]
        public int? F_SortCode { get; set; }
        [DataType("删除标记")]
        public bool? F_DeleteMark { get; set; }
        [DataType("有效标识")]
        public bool? F_EnabledMark { get; set; }
        [DataType("描述")]
        public string F_Description { get; set; }
        [DataType("创建时间")]
        public DateTime? F_CreatorTime { get; set; }
        [DataType("创建用户")]
        public string F_CreatorUserId { get; set; }
        [DataType("最后修改时间")]
        public DateTime? F_LastModifyTime { get; set; }
        [DataType("最后修改用户")]
        public string F_LastModifyUserId { get; set; }
        [DataType("删除时间")]
        public DateTime? F_DeleteTime { get; set; }
        [DataType("删除用户")]
        public string F_DeleteUserId { get; set; }
    }
}
