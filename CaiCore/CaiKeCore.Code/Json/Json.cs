﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CaiKeCore.Code
{
   public static class Json
    {


        public static string ToJson(this object obj)
        {
            var timeConverter = new IsoDateTimeConverter { DateTimeFormat = "yyyy-MM-dd HH:mm:ss" };
            return JsonConvert.SerializeObject(obj, timeConverter);
        }


    }
}
